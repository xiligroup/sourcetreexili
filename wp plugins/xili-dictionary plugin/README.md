# xili-dictionary-plugin
Online translation tool (dictionary) for translation ready plugin and multilingual plugin (xili-language) for WordPress since 2009

The readme file is now one of the files of plugin opened for GitHub contributors to improve this content in english.
When displayed, in [WordPress repository](https://wordpress.org/plugins/xili-dictionary/), the text is divided in tabs.

The other files are available for comments or fixes. Only the version in [WordPress xili-dictionary repository](https://wordpress.org/plugins/xili-dictionary/) is for use.

## Preliminary infos:

1. This readme.txt follow the rules of developers described [here](https://wordpress.org/plugins/about/)
1. Do not forgot to [validate](https://wordpress.org/plugins/about/validator/) the text before commit.

Thanks for your contribution.

M.