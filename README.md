# README #

This repository contains free Wordpress plugins and child themes for multilingual website motorized by WP.

### What is this repository for? ###

* Multilingual trilogy plugins
* Mutlilingual child theme examples of WP bundled themes (2014, 2015, 2016, 2017...)

### Multilingual trilogy plugins ###

* the trilogy contains xili-language, xili-dictionary and xili-tidy-tags.
* these versions are shipped as dev versions

### Mutlilingual child theme examples ###

* The child theme '-xili' here is made to incorporate multilingual features et fixes for better live translation.

* In `functions.php` file, some examples of commented source are shipped to be used in another themes...

* Online demo website is here : http://2017.extend.xiligroup.org

### Who do I talk to? ###

* WordPress website developers
* WordPress contributors